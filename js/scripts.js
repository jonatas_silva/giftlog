/*!
* Start Bootstrap - Landing Page v6.0.4 (https://startbootstrap.com/theme/landing-page)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-landing-page/blob/master/LICENSE)
*/
// This file is intentionally blank
// Use this file to add JavaScript to your project

$(document).ready(function () {
    $('.hover-div').hover(function () {
        $('.hover-div').stop().fadeTo('fast', 0.3);
        $(this).stop().fadeTo('fast', 1);
    }, function () {
        $('.hover-div').stop().fadeTo('fast', 1);
    });
});


/**
 * CONTACT FORM
 */
 const nameEl = document.querySelector("#name");
 const emailEl = document.querySelector("#email");
 const companyNameEl = document.querySelector("#company-name");
 const messageEl = document.querySelector("#message");
 
 const form = document.querySelector("#submit-form");
 
 function checkValidations() {
   let letters = /^[a-zA-Z\s]*$/;
   const name = nameEl.value.trim();
   const email = emailEl.value.trim();
   const companyName = companyNameEl.value.trim();
   const message = messageEl.value.trim();
   if (name === "") {
      document.querySelector(".name-error").classList.add("error");
       document.querySelector(".name-error").innerText =
         "Please fill out this field!";
   } else {
     if (!letters.test(name)) {
       document.querySelector(".name-error").classList.add("error");
       document.querySelector(".name-error").innerText =
         "Please enter only characters!";
     } else {
       
     }
   }
   if (email === "") {
      document.querySelector(".name-error").classList.add("error");
       document.querySelector(".name-error").innerText =
         "Please fill out this field!";
   } else {
     if (!letters.test(name)) {
       document.querySelector(".name-error").classList.add("error");
       document.querySelector(".name-error").innerText =
         "Please enter only characters!";
     } else {
       
     }
   }
 }
 
 function reset() {
   nameEl = "";
   emailEl = "";
   companyNameEl = "";
   messageEl = "";
   document.querySelector(".name-error").innerText = "";
 }

 $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

/**
 * ToolTip-Map
 */
 var myicon = document.getElementById("myicon");
 var mypopup = document.getElementById("mypopup");
 
 myicon.addEventListener("mouseover", showPopup);
 myicon.addEventListener("mouseout", hidePopup);
 
 function showPopup(evt) {
   var iconPos = myicon.getBoundingClientRect();
   mypopup.style.left = (iconPos.right + 20) + "px";
   mypopup.style.top = (window.scrollY + iconPos.top - 60) + "px";
   mypopup.style.display = "block";
 }
 
 function hidePopup(evt) {
   mypopup.style.display = "none";
 }
